# android-automation-3-5



## Yang di perlukan sebelum melakukan automation test
    1. Download java version 17 https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
    2. Downlaod jdk version 17 https://www.oracle.com/java/technologies/downloads/#java17
    3. Install file-file yang sudah di download
    4. Tambahkan ke PATH Environment variable
    5. Download & install node https://nodejs.org/en
    5. Install Appium 
        - Download & Install Git https://git-scm.com/downloads
        - Buka Git Bash
        - Ketik ssh-keygen -t rsa
        - cat c:/Users/yourusername/.ssh/id_rsa.pub
        - copy value and paste to gitlab ssh key setting
        - Pada gitbash ketik npm i --location=global appium
        - Setelah proses selesai, ketik appium
    6. Install UiAutomator2 Driver https://appium.io/docs/en/2.1/quickstart/uiauto2-driver/
    7. Download dan Install Appium Inspector https://appium.io/docs/en/2.1/quickstart/uiauto2-driver/
        - Setting Capability Sets
    8. Download dan Install Intellij https://www.jetbrains.com/idea/download/?section=mac
        - Tambahkan ke PATH Environment variable
    9. Download dan Install Android Studio https://developer.android.com/studio
        - Setup Android Studio https://developer.android.com/design-for-safety/privacy-sandbox/download

## Depedencides untuk menjalankan automation
    1. Java: Bahasa pemrograman yang populer dan platform pengembangan perangkat lunak yang digunakan secara luas untuk pengembangan aplikasi web, mobile, desktop, dan lainnya. Java diketik-statis, objek-berorientasi, dan memiliki platform yang memungkinkan aplikasi untuk dijalankan di berbagai sistem operasi.
    2. JDK (Java Development Kit): Paket perangkat lunak yang mencakup alat-alat yang diperlukan untuk mengembangkan, mengkompilasi, dan menjalankan program Java. JDK berisi Java Runtime Environment (JRE), kompiler Java (javac), pustaka kelas Java (JDK), alat-alat pengembangan, dan dokumentasi.
    3. Menambahkan File Path Environment Variable: Menambahkan lokasi binari atau perangkat lunak ke variabel lingkungan PATH memungkinkan Anda untuk menjalankan perintah dari direktori apa pun di command prompt atau terminal. Ini mempermudah akses ke program atau alat yang terpasang di komputer Anda.
    4. Node.js: Lingkungan runtime JavaScript yang dibangun di atas mesin JavaScript V8 Chrome. Node.js memungkinkan pengembang untuk menulis kode JavaScript di sisi server, bukan hanya di sisi klien. Ini sering digunakan untuk mengembangkan aplikasi web, API, dan server.
    5. Appium: Alat open-source untuk otomatisasi pengujian aplikasi mobile di platform Android dan iOS. Appium memungkinkan pengembang untuk menulis skrip tes menggunakan bahasa pemrograman favorit mereka dan menjalankan tes pada perangkat fisik atau emulator.
    6. Git Bash: Shell yang menciptakan lingkungan berbasis Unix di Windows. Ini menyediakan shell Bash bersama dengan utilitas UNIX yang berguna dan perintah git untuk bekerja dengan repositori git.
    7. UiAutomator2 Driver: Salah satu driver otomatisasi yang didukung oleh Appium untuk mengotomatisasi aplikasi Android. UiAutomator2 Driver menggunakan kerangka kerja UI Automator untuk mengontrol dan menguji aplikasi Android.
    8. Appium Inspector: Alat yang digunakan untuk menginspeksi elemen UI (User Interface) pada aplikasi mobile selama otomatisasi pengujian menggunakan Appium. Appium Inspector memungkinkan pengguna untuk mengidentifikasi elemen dengan mudah dan membuat skrip otomatisasi.
    9. IntelliJ IDEA: Lingkungan pengembangan terintegrasi (IDE) untuk pengembangan perangkat lunak Java, Kotlin, Groovy, Scala, dan Android. IntelliJ IDEA menyediakan berbagai fitur pengembangan yang kuat, termasuk penyusunan kode, refaktorisasi, pengujian, debugging, dan integrasi dengan alat pengembangan lainnya.
    10.Android Studio: IDE resmi untuk pengembangan aplikasi Android, dibangun di atas platform IntelliJ IDEA. Android Studio menyediakan alat pengembangan lengkap untuk pembuatan aplikasi Android, termasuk penyusunan kode, desain UI, debugging, dan pengujian.
    11.Cucumber: Framework pengujian perangkat lunak yang mendukung pengujian otomatis berbasis perilaku (behavior-driven testing) menggunakan skenario yang ditulis dalam format alami. Cucumber memungkinkan kolaborasi antara pengembang dan pemangku kepentingan bisnis dalam menentukan kebutuhan aplikasi dan skenario pengujian.
    12.Gradle: Alat manajemen proyek dan sistem otomatisasi build yang digunakan untuk mengelola dependensi, menyusun, dan membangun proyek perangkat lunak. Gradle sangat populer di ekosistem pengembangan Android dan digunakan untuk membangun aplikasi Android.

## Penjelasan fungsi dari package yang yang di pakai
    1. gradle-wrapper.properties = file konfigurasi yang digunakan oleh Gradle Wrapper. Fungsi utamanya adalah untuk mengkonfigurasi versi Gradle yang akan digunakan oleh Gradle Wrapper saat menjalankan build.
    2. build.gradle = berisi skrip Groovy atau Kotlin yang mendefinisikan konfigurasi proyek, termasuk dependensi, tugas-tugas (tasks), plugin-plugin yang digunakan, dan pengaturan build lainnya.
    3. android_driver = untuk initialize simulator, berisi detail simulator yang dipakai dan fungsi try catch untuk mengatur waitting time
    4. page_object = berisi lokasi element yang mau digunakan dan fungsi action atau perilaku yang mau di lakukan
    5. hooks = untuk menginisialisasi driver Android, dan untuk menampilkan jika ada skenario yang gagal ketika dijalankan
    6. page_definitions = untuk menentukan plugin yang digunakan dan untuk menentukan lokasi file fitur yang dijalankan dan untuk menerima scenario cucumber yang dibuat dan memproses nya dalam bentuk perintah program
    7. Features = lokasi dimana scenario cucumber dibuat

## UI Map/Object Repository
    UI Map atau Object Repository adalah sebuah konsep dalam automation testing yang digunakan untuk menyimpan dan mengelola informasi tentang elemen-elemen UI (User Interface) dari aplikasi yang akan diuji. Ini bisa berupa berbagai jenis elemen seperti tombol, teks, dropdown, form, dan lainnya yang ada di halaman aplikasi.
    
    Object Repository biasanya terdiri dari entri-entri yang mengidentifikasi setiap elemen UI, seperti ID, nama, kelas, XPath, atau selektor CSS. Setiap entri ini terkait dengan sebuah nama yang dapat digunakan dalam skrip pengujian, sehingga memudahkan pengelolaan dan pemeliharaan skrip pengujian.

    Ada beberapa alasan mengapa UI Map/Object Repository sangat diperlukan dalam automation testing:
    1. Memisahkan Logika Pengujian dari Implementasi UI: Dengan menggunakan UI Map/Object Repository, logika pengujian (test scripts) tidak perlu bergantung langsung pada implementasi UI (elemen-elemen HTML/XML). Ini memungkinkan perubahan dalam UI tanpa mempengaruhi skrip pengujian, karena cukup memperbarui entri dalam Repository.
    2. Pemeliharaan yang Mudah: Object Repository memungkinkan untuk menyimpan semua identifikasi elemen UI dalam satu lokasi terpusat. Ini membuatnya lebih mudah untuk mengelola, memperbarui, atau mengubah entri-entri ketika ada perubahan pada aplikasi.
    3. Reusable Components: Dengan menggunakan Object Repository, elemen-elemen UI dapat digunakan kembali di berbagai skrip pengujian. Ini mengurangi duplikasi kode dan memungkinkan untuk meningkatkan efisiensi dalam pengembangan skrip pengujian.
    4. Mudah dipahami dan Dikelola: Dengan memisahkan logika pengujian dari implementasi UI, Object Repository membuat skrip pengujian lebih mudah dibaca, dipahami, dan dikelola oleh tim pengembang dan pengujian.

## Manfaat penggunaan UI Map/Object Repository
    Keuntungan utama penggunaan repositori objek adalah pemisahan objek dari kasus uji.
    Jika nilai penentu lokasi dari satu elemen web/aplikasi berubah, hanya repositori objek yang perlu diubah daripada melakukan perubahan pada semua kasus uji di mana penentu lokasinya telah digunakan.
    Menjaga repositori objek meningkatkan modularitas implementasi kerangka kerja.

## Step implementasi UI Map/Object Repository

    1. Buat folder utils di src/main/java/demo.
    2. Buat folder elements di src/test/resources.
    3. Buat kelas Java dengan nama Constants di dalam folder utils, dan salin kode yang diberikan pada tautan: https://pastebin.com/MtX5LyPH.
    4. Buat kelas Java dengan nama Utils di dalam folder utils, dan salin kode yang diberikan pada tautan: https://pastebin.com/d97TPhT5.
    5. Tambahkan loadElementProperties(Constants.ELEMENTS) di dalam hook @Before.
    6. Implementasikan BasePageObject sesuai dengan kode yang diberikan pada tautan: https://pastebin.com/WNumPEm3.
    Dengan mengikuti langkah-langkah di atas, kita akan membuat struktur folder dan kelas-kelas Java yang diperlukan untuk mengelola konstan dan utilitas dalam proyek kita.
##  T    A   M   A   T   #