package stockbit.page_object;

import org.openqa.selenium.By;
import javax.accessibility.AccessibleStateSet;
import static stockbit.utils.Utils.env;

public class LoginPage extends BasePage{
    public void isOnboardingPage() {
        assertIsDisplay("ICON_STOCKBIT");
    }

    public void tapLogin() {
        tap("BUTTON_ENTRY_LOGIN");
    }

    public void inputUsername(String username){
        typeOn("FIELD_USERNAME", env(username));
    }

    public void inputPassword(String password) {
        typeOn("FIELD_PASSWORD", env(password));
    }

    public void tapSubmitLoginButton(){
        tap("BUTTON_SUBMIT_LOGIN");
    }

    public void tapSkipBiometricPopup() {
        tap("BUTTON_SKIP_BIOMETRIC");
    }

    public void tapCloseBanner() {
        tap("BUTTON_CLOSE_BANNER");
    }

    public void tapSkipAvatar() {
        tap("BUTTON_SKIP_AVATAR");
    }

    public void isWatchlistPage() {
        tapSkipBiometricPopup();
        tapSkipAvatar();
        tapCloseBanner();
        assertIsDisplayed(By.xpath("//android.widget.TextView[@resource-id='com.stockbit.android:id/tv_name' and @text='All Watchlist']"));
    }
}
