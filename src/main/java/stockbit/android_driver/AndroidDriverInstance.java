package stockbit.android_driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AndroidDriverInstance {
    public static AndroidDriver androidDriver;

    public static void initialize(){
        UiAutomator2Options caps = new UiAutomator2Options();

        caps.setPlatformName("12");
        caps.setUdid("emulator-5554");
        caps.setApp("C:/Kevin's Data/bootcamp/2.20.2-rc3(10800).apk");
        caps.setAppWaitActivity("*");
       /* caps.setPlatformName("Android");
        caps.setPlatformVersion("8.1.0");
        caps.setUdid("emulator-5554");
        caps.setApp("/Users/Melliana Astrid/Downloads/2.20.2-rc3(10800).apk");
        caps.setAppWaitActivity("*");*/

        try {
            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723"),caps);
            androidDriver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        }catch (MalformedURLException e){
            throw new RuntimeException(e);
        }
    }

    public static void quit(){
        androidDriver.quit();
    }
}
